import { NestFactory } from "@nestjs/core"
import "colors"
import { AppModule } from "./app.module"

const PORT = process.env.PORT || 5000

async function bootstrap() {
	const app = await NestFactory.create(AppModule, { cors: true })
	await app.listen(PORT)
	console.log(`Server is running in ${process.env.NODE_ENV} mode on ${PORT} port.`.cyan.bold)
}

bootstrap()
