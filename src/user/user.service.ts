import { BadRequestException, Injectable } from "@nestjs/common"
import * as mongoose from "mongoose"
import { Model } from "mongoose"
import { InjectModel } from "@nestjs/mongoose"
import * as bcrypt from "bcrypt"

import { User, UserDocument } from "./user.schema"
import { UserDto } from "./DTO/user.dto"
import { NOT_FOUND_USER_ON_EMAIL_ERROR, NOT_FOUND_USER_ON_ID_ERROR, USER_FOUND_ERROR } from "./user.constants"

@Injectable()
export class UserService {
	constructor(@InjectModel(User.name) private readonly userModel: Model<UserDocument>) {
	}

	public async getAllUsers() {
		const users = await this.userModel.aggregate([
			{
				$lookup: {
					from: "anime",
					foreignField: "userId",
					localField: "_id",
					as: "anime"
				}
			},
			{
				$project: {
					_id: 1,
					login: 1,
					email: 1,
					password: 1,
					anime: 1
				}
			}
		])

		const allUsers = {
			users,
			count: await this.userModel.countDocuments()
		}
		return allUsers
	}

	public async getUserById(id: string) {
		const hasUser = await this.userModel.findOne({ _id: id })

		if (!hasUser) throw new BadRequestException(NOT_FOUND_USER_ON_ID_ERROR)

		const user = await this.userModel.aggregate([
			{
				$match: { _id: new mongoose.Types.ObjectId(id) }
			},
			{
				$lookup: {
					from: "anime",
					foreignField: "userId",
					localField: "_id",
					as: "anime"
				}
			},
			{
				$project: {
					_id: 1,
					login: 1,
					email: 1,
					password: 1,
					anime: 1
				}
			}
		])

		return user
	}

	public async registrationUser(user: UserDto) {
		const { login, password, email } = user
		const hash = await this.hashPassword(password, 10)
		const hasUser = await this.userModel.findOne({ email })

		if (hasUser) throw new BadRequestException(USER_FOUND_ERROR)

		const newUser = new this.userModel({
			login,
			email,
			password: hash
		})
		return newUser.save()
	}

	public async forgotPassword(user: UserDto) {
		const { login, email, password } = user

		const hasUser = await this.userModel.findOne({ email, login })

		if (!hasUser) throw new BadRequestException(NOT_FOUND_USER_ON_EMAIL_ERROR)

		const passwordHash = await this.hashPassword(password, 10)
		user.password = passwordHash

		const userUpdated = await this.userModel.findOneAndUpdate({ email }, user)

		return userUpdated
	}

	public async removeUser(id: string) {
		const hasUser = await this.userModel.findById(id)

		if (!hasUser) throw new BadRequestException(NOT_FOUND_USER_ON_ID_ERROR)

		return this.userModel.findByIdAndRemove(id)
	}

	public async removeAllUsers() {
		return this.userModel.deleteMany({})
	}

	public async hashPassword(enteredPassword: string, rounds: number) {
		const salt = await bcrypt.genSalt(rounds)
		const hash = await bcrypt.hash(enteredPassword, salt)

		return hash
	}
}
