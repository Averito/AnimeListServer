import { IsNotEmpty, IsString } from "class-validator"

export class UserDto {
	@IsNotEmpty()
	@IsString()
	login: string

	@IsNotEmpty()
	@IsString()
	email: string

	@IsNotEmpty()
	@IsString()
	password: string
}