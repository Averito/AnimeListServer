import { Body, Controller, Delete, Get, Param, Post, Query, UseGuards, UsePipes, ValidationPipe } from "@nestjs/common"

import { UserService } from "./user.service"
import { UserDto } from "./DTO/user.dto"
import { JwtAuthGuard } from "../auth/guards/jwt.guard"

@Controller("users")
export class UserController {
	constructor(private readonly userService: UserService) {
	}

	@Get()
	@UseGuards(JwtAuthGuard)
	getAllUsers() {
		return this.userService.getAllUsers()
	}

	@Get(":id")
	@UseGuards(JwtAuthGuard)
	@UsePipes(new ValidationPipe({ transform: true }))
	getUserbyId(@Param() param) {
		return this.userService.getUserById(param.id)
	}

	@Post("registration")
	@UsePipes(new ValidationPipe({ transform: true }))
	registrationUser(@Body() user: UserDto) {
		return this.userService.registrationUser(user)
	}

	@Post("forgot-password")
	@UsePipes(new ValidationPipe({ transform: true }))
	forgotPasswordUser(@Body() user: UserDto) {
		return this.userService.forgotPassword(user)
	}

	@Delete(":id")
	@UseGuards(JwtAuthGuard)
	@UsePipes(new ValidationPipe({ transform: true }))
	removeUser(@Param() param) {
		return this.userService.removeUser(param.id)
	}

	@Delete()
	@UseGuards(JwtAuthGuard)
	removeAllUsers() {
		return this.userService.removeAllUsers()
	}
}
