import { Module } from "@nestjs/common"
import { ConfigModule, ConfigService } from "@nestjs/config"
import { MongooseModule } from "@nestjs/mongoose"

import { UserModule } from "./user/user.module"
import { AuthModule } from "./auth/auth.module"
import { getMongoDbConfig } from "./config/mongo.config"
import { AnimeModule } from "./anime/anime.module"

@Module({
	imports: [ConfigModule.forRoot(), MongooseModule.forRootAsync({
		imports: [ConfigModule],
		inject: [ConfigService],
		useFactory: getMongoDbConfig
	}), UserModule, AuthModule, AnimeModule]
})
export class AppModule {
}
