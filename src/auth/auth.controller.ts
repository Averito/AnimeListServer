import { Body, Controller, Get, Post, Req, UseGuards } from "@nestjs/common"

import { AuthService } from "./auth.service"
import { UserDto } from "../user/DTO/user.dto"
import { JwtAuthGuard } from "./guards/jwt.guard"

@Controller()
export class AuthController {
	constructor(private readonly authService: AuthService) {
	}

	@Post("auth")
	async loginUser(@Body() user: UserDto) {
		const { id, login, email } = await this.authService.validateUser(user)
		return this.authService.authUser(id, login, email)
	}

	@Get("auth-check")
	@UseGuards(JwtAuthGuard)
	async authCheck(@Req() req) {
		return this.authService.checkAuth(req.headers.authorization.split(" ")[1])
	}
}
