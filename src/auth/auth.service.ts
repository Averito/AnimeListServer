import { BadRequestException, Injectable, UnauthorizedException } from "@nestjs/common"
import { InjectModel } from "@nestjs/mongoose"
import { Model } from "mongoose"
import { JwtService } from "@nestjs/jwt"
import { compare } from "bcrypt"

import { UserDto } from "../user/DTO/user.dto"
import { User, UserDocument } from "../user/user.schema"
import { UserService } from "../user/user.service"
import { EXPIRED_TOKEN_ERROR, NOT_FOUND_USER_ERROR, UNVERIFY_TOKEN_ERROR, WRONG_PASSWORD } from "./auth.constants"


@Injectable()
export class AuthService {
	constructor(@InjectModel(User.name) private readonly userModel: Model<UserDocument>,
							private readonly JwtService: JwtService,
							private readonly userService: UserService) {
	}

	public async authUser(id: string, login: string, email: string) {
		const access_token = await this.generateToken(id, login, email)
		const userId = await this.userModel.findOne({ _id: id })

		return { access_token, userId: userId._id }
	}

	public async validateUser(user: UserDto) {
		const hasUser = await this.userModel.findOne({ email: user.email })

		if (!hasUser) throw new BadRequestException(NOT_FOUND_USER_ERROR)

		const { hash } = await this.userService.hashPassword(user.password, 10)
		const isValidPassword = await compare(user.password, hasUser.password)

		if (!isValidPassword) throw new BadRequestException(WRONG_PASSWORD)

		return { id: hasUser._id, login: hasUser.login, email: hasUser.email }
	}

	public async checkAuth(token: string) {
		try {
			const valid = this.JwtService.verify(token, { secret: process.env.JWT_KEY })

			if (!valid) throw new BadRequestException(UNVERIFY_TOKEN_ERROR)

			return valid
		} catch (e) {
			throw new UnauthorizedException(EXPIRED_TOKEN_ERROR)
		}
	}

	private async generateToken(id: string, login: string, email: string) {
		const payload = { id, login, email }
		return await this.JwtService.signAsync(payload)
	}
}
