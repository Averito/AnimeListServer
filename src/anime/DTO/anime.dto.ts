import { IsEnum, IsNotEmpty, IsNumber, IsString } from "class-validator"

enum statuses {
	"Просмотренно",
	"Смотрю",
	"Запланированно",
	"Выходит",
	"Заброшено"
}

export class AnimeDto {
	@IsNotEmpty()
	@IsString()
	name: string

	@IsNotEmpty()
	@IsEnum(statuses)
	status: number
}