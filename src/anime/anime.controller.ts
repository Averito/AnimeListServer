import {
	Body,
	Controller,
	Delete,
	Get,
	Param,
	Post,
	Put,
	Query,
	UseGuards,
	UsePipes,
	ValidationPipe
} from "@nestjs/common"

import { JwtAuthGuard } from "../auth/guards/jwt.guard"
import { AnimeService } from "./anime.service"
import { AnimeDto } from "./DTO/anime.dto"

@Controller("anime")
export class AnimeController {
	constructor(private readonly animeService: AnimeService) {}

	@Get()
	@UseGuards(JwtAuthGuard)
	getAllAnime() {
		return this.animeService.getAllAnime()
	}

	@Get(":id")
	@UseGuards(JwtAuthGuard)
	getAnimeByUserId(@Param() params) {
		return this.animeService.getAnimeByUserId(params.id)
	}

	@Post()
	@UseGuards(JwtAuthGuard)
	@UsePipes(new ValidationPipe({transform: true}))
	createAnime(@Body() anime: AnimeDto) {
		return this.animeService.createAnime(anime)
	}

	@Put(":id")
	@UseGuards(JwtAuthGuard)
	editStatusAnime(@Param() params, @Body() anime: AnimeDto) {
		return this.animeService.editStatusAnime(params.id, anime)
	}

	@Delete()
	@UseGuards(JwtAuthGuard)
	deleteAllAnime() {
		return this.animeService.deleteAllAnime()
	}

	@Delete(":id")
	@UseGuards(JwtAuthGuard)
	@UsePipes(new ValidationPipe({transform: true}))
	deleteAnimeById(@Param() params) {
		return this.animeService.deleteAnimeById(params.id)
	}
}
